#include <includes.h>

OS_TCB LedTask1TCB;
CPU_STK LedTask1Stk[APP_CFG_TASK_START_STK_SIZE];

OS_TCB LedTask2TCB;
CPU_STK LedTask2Stk[APP_CFG_TASK_START_STK_SIZE];


void LedTask1(void);
void LedTask2(void);

int main()
{
  OS_ERR err;
  BSP_IntDisAll();
  
  CPU_Init();
  Mem_Init();
  
  OSInit(&err);
  
  OSTaskCreate((OS_TCB*)&LedTask1TCB,
               (CPU_CHAR*)"LedTask1",
               (OS_TASK_PTR)LedTask1,
               (void*)0u,
               (OS_PRIO)APP_CFG_TASK_START_PRIO,
               (CPU_STK*)&LedTask1Stk[0u],
               (CPU_STK_SIZE)APP_CFG_TASK_START_STK_SIZE/10,
               (CPU_STK_SIZE)APP_CFG_TASK_START_STK_SIZE,
               (OS_MSG_QTY)0u,
               (OS_TICK)0u,
               (void*)0u,
               (OS_OPT)(OS_OPT_TASK_STK_CHK|OS_OPT_TASK_STK_CLR),
               (OS_ERR*)&err);
  
  OSStart(&err);
}

void LedTask1(void)
{
  OS_ERR  err;
  BSP_Init();
  BSP_Tick_Init();
  BSP_LED_Off(0u);
  
  OSTaskCreate((OS_TCB*)&LedTask2TCB,
               (CPU_CHAR*)"LedTask2",
               (OS_TASK_PTR)LedTask2,
               (void*)0u,
               (OS_PRIO)APP_CFG_TASK_START_PRIO+1,
               (CPU_STK*)&LedTask2Stk[0u],
               (CPU_STK_SIZE)APP_CFG_TASK_START_STK_SIZE/10,
               (CPU_STK_SIZE)APP_CFG_TASK_START_STK_SIZE,
               (OS_MSG_QTY)0u,
               (OS_TICK)0u,
               (void*)0u,
               (OS_OPT)(OS_OPT_TASK_STK_CHK|OS_OPT_TASK_STK_CLR),
               (OS_ERR*)&err);
  while(DEF_TRUE){
    BSP_LED_Toggle(3u);
    OSTimeDlyHMSM(0u,0u,0u,1000u,OS_OPT_TIME_HMSM_STRICT,&err);
  } 
}

void LedTask2(void)
{
  OS_ERR  err;
  while(DEF_TRUE){
    BSP_LED_Toggle(4u);
    OSTimeDlyHMSM(0u,0u,0u,20400u,OS_OPT_TIME_HMSM_STRICT,&err);

  }
}
